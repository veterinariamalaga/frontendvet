/** @type {import('tailwindcss').Config} */
export default {
	content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}', './src/*.{css,tsx}'],
	theme: {
		extend: {},
	},
	plugins: [],
}
